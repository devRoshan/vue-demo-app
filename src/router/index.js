import Vue from 'vue'
import VueRouter from 'vue-router'

import Add from '../views/Add.vue'
import Todos from '../views/Todos.vue'
import Todo from '../views/Todo.vue'
import Edit from '../views/Edit.vue'
import RandomNumberTrivia from '../views/RandomNumberTrivia.vue'
import Weather from '../views/Weather.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Todos',
    component: Todos
  },
  {
    path: '/todo/:id',
    name: 'Todo',
    component: Todo
  },
  {
    path: '/todo/add',
    name: 'Add',
    component: Add
  },
  {
    path: '/todo/:id/edit',
    name: 'Edit',
    component: Edit
  },
  {
    path: '/random-number-trivia',
    name: 'RandomNumberTrivia',
    component: RandomNumberTrivia
  },
  {
    path: '/weather',
    name: 'Weather',
    component: Weather
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
