const getters = {
  getTodos(state) {
    return state.todos
  }
}

export default getters
