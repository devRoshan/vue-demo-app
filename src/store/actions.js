const actions = {
  addTodo (context, data) {
    context.commit('addTodo', data)
  },
  deleteTodo(context, data) {
    context.commit('deleteTodo', data)
  },
  updateTodo (context, data) {
    context.commit('updateTodo', data)
  }
}

export default actions
