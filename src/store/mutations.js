import { v4 as uuidv4 } from 'uuid'

const mutations = {
  addTodo (state, todo) {
    const id = uuidv4()
    let _todo = Object.assign(todo, {id: id})

    state.todos.push(_todo)
    
    localStorage.setItem('todos', JSON.stringify(state.todos))
  },
  deleteTodo(state, data) {
    state.todos.forEach(function (item, index) {
      if (item.id == data.id) {
        state.todos.splice(index, 1)
      }
    })

    localStorage.setItem('todos', JSON.stringify(state.todos))
  },
  updateTodo (state, data) {
    state.todos.forEach(function (item, index) {
      if (item.id == data.id) {
        state.todos[index] = data
      }
    })

    localStorage.setItem('todos', JSON.stringify(state.todos))
  }
}

export default mutations
