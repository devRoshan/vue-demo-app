const state = {
  todos: localStorage.getItem('todos') ? JSON.parse(localStorage.getItem('todos')) : []
}

export default state
